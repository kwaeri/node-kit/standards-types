/**
 * SPDX-PackageName: kwaeri/standards-types
 * SPDX-PackageVersion: 0.7.0
 * SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
 */


 'use strict'


// INCLUDES


// DEFINES

/* A type for error messages ( Google JSON Style Guide ) */
export type ErrorMessageBits = {
    domain?: string;
    reason?: string;
    message?: string;
    location?: string;
    locationType?: string;
    extendedHelp?: string;
    sendReport?: string;
}

/* A type for error collections ( Google JSON Style Guide ) */
export type ErrorBits = {
    code?: number;
    message?: string;
    errors?: ErrorMessageBits[];
}

/* A type for parameter collections ( Google JSON Style Guide ) */
export type ParameterBits = {
    [key: string]: string;
}

/* A type for record item collections ( Google JSON Style Guide ) */
export type ItemsBits = {
    [key: string]: any;
}

/* A type for request data ( Google JSON Style Guide ) */
export type DataBits = {
    kind?: string;
    fields?: string;
    etag?: string;
    id?: string;
    lang?: string;
    updated?: string;   // date formatted RFC 3339
    deleted?: boolean;
    currentItemCount?: number;
    itemsPerPage?: number;
    startIndex?: number;
    totalItems?: number;
    pageIndex?: number;
    totalPages?: number;
    pageLinkTemplate?: string; // /^https?:/ ?;
    next?: any; // {}*
    nextLink?: string;
    previous?: any; // {}*
    previousLink?: string;
    self?: any; // {}*
    selfLink?: string;
    edit?: any; // {}*
    editLink?: string;
    items?: ItemsBits[];
}

/* A type for network communication  ( Google JSON Style Guide ) */
export type RequestBits = {
    apiVersion?: string;
    context?: string;
    id?: string;
    method?: string;
    params?: ParameterBits;
    data?: DataBits
    error?: ErrorBits;
}

/* An alias to the RequestBits for elegance */
export type ResponseBits = RequestBits;

