/**
 * SPDX-PackageName: kwaeri/standards-types
 * SPDX-PackageVersion: 0.7.0
 * SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
 */


'use strict'

/* Some types for logic guidance and type hinting: */
export namespace KWAERI {
    export namespace ENV {
        export type DEFAULT = "default";
        export type PRODUCTION = "production";
        export type TEST = "test";
    }
};

export type NodeKitProjectAuthorBits = {
    fullName: string;
    first: string;
    last: string;
    email: string;
};

export type NodeKitProjectLicenseBits = {
    identifier: string;
    name?: string;
    text?: string;
};

export type NodeKitProjectBits = {
    name: string;
    fileSafeName?: string;
    type: string;
    tech: string;
    root: string;
    repository: string;
    author: NodeKitProjectAuthorBits;
    copyright: string;
    copyrightEmail: string;
    license: NodeKitProjectLicenseBits;
};

export type NodeKitConfigurationBits = {
    project: NodeKitProjectBits;
};

export type NodeKitOptions = {
    environment?: KWAERI.ENV.PRODUCTION | KWAERI.ENV.DEFAULT | KWAERI.ENV.TEST;
    quest: string;
    specification: string;
    subCommands: string[];
    args: any;
    version:string;
    configuration?: NodeKitConfigurationBits
};